export const MESSAGE_TOAST = {
  SUCCESS_SEARCH: "Search successfully",
  SUCCESS_ADD: "Add successfully",
  SUCCESS_PASSWORD_UPDATE: "Update password successfully",
  SUCCESS_CANCEL: "Cancel successfully",
  SUCCESS_MODIFY: "Modify successfully",

  ERROR_PASSWORD_UPDATE: "Incorrect current password",
};

export const KEY_LOCAL_STORAGE = {
  AUTHEN: "objAuthen",
};
