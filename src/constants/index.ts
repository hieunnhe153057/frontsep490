import OverviewDoctor from "../pages/Doctor/Detail/OverviewDoctor";
import ScheduleDoctor from "../pages/Doctor/Detail/ScheduleDoctor";
import InformationPatient from "../pages/Patient/Detail/InformationPatient";

export const RouterUrl = {
  DASHBOARD: "/dashboard",
  DOCTOR: "/doctor",
  PATIENT: "/patient",
  DEPARTMENT: "/department",
  APPOINTMENT: "/appointment",
  DIAGNOSTIC_REPORT: "/diagnostic-report",
  STAFF: "/staff",
  LOGIN: "/login",
  REGISTER: "/register",
};

export const START_PAGE = 1;

export const DEFAULT_ITEM_PER_PAGE = 10;

export const LIST_OPTION_PAGINATION = [
  {
    title: "10",
    value: 10,
  },
  {
    title: "25",
    value: 25,
  },
  {
    title: "50",
    value: 50,
  },
  {
    title: "100",
    value: 100,
  },
];

export const LIST_OPTION_PAGINATION_FULL = [
  {
    title: "10",
    value: 10,
  },
  {
    title: "25",
    value: 25,
  },
  {
    title: "50",
    value: 50,
  },
  {
    title: "100",
    value: 100,
  },
  {
    title: "All",
    value: 150,
  },
];

export const MenuDataDoctor = [
  {
    title: "Overview",
    content: OverviewDoctor,
  },
  {
    title: "Schedule",
    content: ScheduleDoctor,
  },
];

export const MenuDataPatient = [
  {
    title: "Information",
    content: InformationPatient,
  },
];

export const GENDER = [
  {
    name: "Male",
    code: 0,
  },
  {
    name: "Female",
    code: 1,
  },
  {
    name: "Other",
    code: 2,
  },
];
