import { ReactNode } from "react";

export interface MenuItem {
    title: string;
    subMenu?: MenuItem[];
    content: any
}