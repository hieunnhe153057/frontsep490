import {
  ICON_DEPARTMENT,
  ICON_DOCTOR,
  ICON_PATIENT,
  ICON_STAFF,
} from "../../assets";

function TotalView() {
  return (
    <section className="totalview">
      <div className="container">
        <div className="row">
          <div className="col-3">
            <div className="d-flex totalview-box p-2">
              <p className="w-50 d-flex justify-content-center align-item-center m-auto">
                <ICON_DOCTOR style={{ width: "50px", height: "50px" }} />
              </p>
              <div className="w-50 m-auto">
                <p>
                  Total <span className="d-block">Doctors</span>
                </p>
                <p>80/80</p>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="d-flex totalview-box p-2">
              <p className="w-50 d-flex justify-content-center align-item-center m-auto">
                <ICON_STAFF style={{ width: "50px", height: "50px" }} />
              </p>
              <div className="w-50 m-auto">
                <p>
                  Total <span className="d-block">Staff</span>
                </p>
                <p>20/20</p>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="d-flex totalview-box p-2">
              <p className="w-50 d-flex justify-content-center align-item-center m-auto">
                <ICON_DEPARTMENT style={{ width: "50px", height: "50px" }} />
              </p>
              <div className="w-50 m-auto">
                <p>
                  Total <span className="d-block">Department</span>
                </p>
                <p>20</p>
              </div>
            </div>
          </div>
          <div className="col-3 ">
            <div className="d-flex totalview-box p-2">
              <p className="w-50 d-flex justify-content-center align-item-center m-auto">
                <ICON_PATIENT style={{ width: "50px", height: "50px" }} />
              </p>
              <div className="w-50 m-auto">
                <p>
                  Total <span className="d-block">Patient</span>
                </p>
                <p>1000</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default TotalView;
