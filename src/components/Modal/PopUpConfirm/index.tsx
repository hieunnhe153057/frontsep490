

import { Button, Modal } from 'react-bootstrap';
import { useState } from 'react';

interface IPropsConfirm {
    handleCloseConfirmPopup: (value: boolean) => void;
}

const PopUpConfirm = (props: IPropsConfirm) => {
    const { handleCloseConfirmPopup} = props;

    const [user, setUser] = useState('')

    return <>
        <Modal centered show={true} onHide={() => { handleCloseConfirmPopup(false) }}>
            {/* <Modal.Header closeButton style={{ background: "#16365c", color: "#fff" }}>
                <Modal.Title>
                    <span className='h5'>Cancel All Confirmation</span>
                </Modal.Title>
            </Modal.Header> */}
            <Modal.Body className='mt-2 mb-2'>
                <span><i className="bi bi-exclamation-circle text-warning"></i></span>
            <span className='ms-3 fs-18 fw-600 text-center'>Are you sure to delete <span className='fw-bold'>{user}</span> in Staff list？</span>
            </Modal.Body>
            <Modal.Footer className='justify-content-center'>
                <Button className='button button--small button--outline' onClick={() => { handleCloseConfirmPopup(false) }}>
                    No
                </Button>
                <Button className='button button--small button--primary'>
                    Yes
                </Button>
            </Modal.Footer>
        </Modal>
    </>;
}
export default PopUpConfirm;