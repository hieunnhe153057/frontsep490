import { memo } from "react";

import { MenuDataDoctor } from "../../../constants";
import MenuHeader from "../../../components/common/MenuHeader";

const DetailDoctor = () => {
  return (
    <>
      <MenuHeader menuData={MenuDataDoctor} />
      <div className="blockmodule-mainbody table-responsive mt-4">
        table data
      </div>
    </>
  );
};

export default memo(DetailDoctor);
