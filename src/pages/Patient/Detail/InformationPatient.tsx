import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { GENDER } from "../../../constants";

interface IPropInformationPatient {}

const SignupSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  lastName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
});

const InformationPatient = (props: IPropInformationPatient) => {
  return (
    <section className="info-patient">
      <div className="info-patient-title mb-3">
        <h3>add new</h3>
      </div>

      <div className="info-patient-content">
        <p className="fs-5">Basic information</p>
        <Formik
          initialValues={{
            name: "",
            birthday: "",
            gender: "",
            phoneNumber: "",
            email: "",
            residence: "",
            city: "",
          }}
          
          validationSchema={SignupSchema}
          onSubmit={(values) => {
            // same shape as initial values
            console.log(values);
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <div>
                <div className="row">
                  <div className="col-12 mb-3">
                    <label htmlFor="name">
                      Name <span className="text-danger">*</span>
                    </label>
                    <Field name="name" id="name" className="form-control" />
                    {errors.name && touched.name ? (
                      <div>{errors.name}</div>
                    ) : null}
                  </div>
                  <div className="col-6 mb-3">
                    <label htmlFor="birthday">
                      Date of birth <span className="text-danger">*</span>
                    </label>
                    <input
                      type="date"
                      className="form-control input-select"
                      value={""}
                      max="9999-12-31"
                      // onChange={(event) => handleChangeFromDate(event.target.value)}
                    />
                    {errors.birthday && touched.birthday ? (
                      <div>{errors.birthday}</div>
                    ) : null}
                  </div>
                  <div className="col-6 mb-3">
                    <label htmlFor="gender">
                      Gender <span className="text-danger">*</span>
                    </label>
                    <Field
                      as="select"
                      name="gender"
                      id="gender"
                      className="form-select"
                    >
                      {GENDER.map((item: any) => (
                        <option value={item.code} key={item.code}>
                          {item.name}
                        </option>
                      ))}
                    </Field>
                    {errors.gender && touched.gender ? (
                      <div>{errors.gender}</div>
                    ) : null}
                  </div>
                  <div className="col-6 mb-3">
                    <label htmlFor="residence">
                      Current residence <span className="text-danger">*</span>
                    </label>
                    <Field
                      name="residence"
                      type="text"
                      id="residence"
                      className="form-control"
                    />
                    {errors.residence && touched.residence ? (
                      <div>{errors.residence}</div>
                    ) : null}
                  </div>
                  <div className="col-6 mb-3">
                    <label htmlFor="city">
                      Citizen identification{" "}
                      <span className="text-danger">*</span>
                    </label>
                    <Field
                      name="city"
                      type="text"
                      id="city"
                      className="form-control"
                    />
                    {errors.city && touched.city ? (
                      <div>{errors.city}</div>
                    ) : null}
                  </div>
                  <div className="col-6 mb-3">
                    <label htmlFor="phoneNumber">
                      Phone number <span className="text-danger">*</span>
                    </label>
                    <Field
                      name="Phone number"
                      type="text"
                      id="phoneNumber"
                      className="form-control"
                    />
                    {errors.phoneNumber && touched.phoneNumber ? (
                      <div>{errors.phoneNumber}</div>
                    ) : null}
                  </div>
                  <div className="col-6 mb-3">
                    <label htmlFor="email">
                      Email address <span className="text-danger">*</span>
                    </label>
                    <Field
                      name="email"
                      type="email"
                      id="email"
                      className="form-control"
                    />
                    {errors.email && touched.email ? (
                      <div>{errors.email}</div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="mt-3 d-block ms-auto">
                <button
                  type="submit"
                  className="button button--small button--danger me-3"
                  style={{ width: "10%" }}
                >
                  Delete
                </button>

                <button
                  type="submit"
                  className="button button--small button--primary"
                  style={{ width: "10%" }}
                >
                  Save
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </section>
  );
};

export default InformationPatient;
