import { memo, useRef, useState } from "react";
import Layout from "../../components/Layout";
import TotalView from "../../components/common/TotalView";
import PopUpConfirm from "../../components/Modal/PopUpConfirm";
import MaterialTable from "material-table";
import {
  Autocomplete,
  TextField,
  ThemeProvider,
  createTheme,
} from "@mui/material";

import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { GENDER } from "../../constants";
import PaginationComponent from "../../components/common/Pagination";
import Detail from "./Detail";

const Patient = () => {
  const [showPopUpConfirm, setShowPopUpConfirm] = useState(false);
  const [listData, setListData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemPerPage, setItemPerPage] = useState(10);
  const [totalItem, setTotalItem] = useState<number>(100);

  const [departmentList, setDepartmentList] = useState([]);

  const [name, setName] = useState("");
  const [gender, setGender] = useState(-1);
  const [department, setDepartment] = useState("");

  const [isOpenDetail, setIsOpenDetail] = useState(false);

  const handleChangeName = (value: string) => {};

  const handleChangeGender = (value: string) => {};

  const handleChangeDepartment = (event: any, values: any) => {};

  const handleKeyUpDepartment = (event: any) => {};

  const handleCancel = (item: any) => {};

  const handleModify = (item: any) => {};

  const getCurrentPage = (item: number) => {
    setCurrentPage(item);
  };

  const getItemPerPage = (item: number) => {
    setItemPerPage(item);
    setCurrentPage(1);
  };

  const _renderTableListPatient = () => {
    const defaultMaterialTheme = createTheme();

    return (
      <ThemeProvider theme={defaultMaterialTheme}>
        <MaterialTable
          style={{ boxShadow: "none" }}
          localization={{
            header: {
              actions: "",
            },
          }}
          columns={[
            { title: "Name", field: "name" },
            { title: "Gender", field: "gender" },
            { title: "Date of Birth", field: "birthYear", type: "numeric" },
            {
              title: "Phone number",
              field: "phone",
            },
            {
              title: "Email",
              field: "email",
            },
          ]}
          data={[
            {
              name: "Mehmet",
              gender: "Baran",
              birthYear: 1987,
              phone: 123123123123,
              email: "asdasdasdasd",
            },
            {
              name: "Zerya Betül",
              gender: "asdasds",
              birthYear: 2017,
              phone: 123123123123,
              email: "asdasdasdasd",
            },
            {
              name: "Zerya Betül",
              gender: "asdasds",
              birthYear: 2017,
              phone: 123123123123,
              email: "asdasdasdasd",
            },
            {
              name: "Zerya Betül",
              gender: "asdasds",
              birthYear: 2017,
              phone: 123123123123,
              email: "asdasdasdasd",
            },
            {
              name: "Zerya Betül",
              gender: "asdasds",
              birthYear: 2017,
              phone: 123123123123,
              email: "asdasdasdasd",
            },
            {
              name: "Zerya Betül",
              gender: "asdasds",
              birthYear: 2017,
              phone: 123123123123,
              email: "asdasdasdasd",
            },
            {
              name: "Zerya Betül",
              gender: "asdasds",
              birthYear: 2017,
              phone: 123123123123,
              email: "asdasdasdasd",
            },
          ]}
          options={{
            search: false,
            sorting: false,
            showTitle: false,
            paging: false,
            showTextRowsSelected: false,
            actionsColumnIndex: -1,
            selection: true,
            toolbar: false,
          }}
          actions={[
            {
              icon: DeleteOutlineOutlinedIcon,
              onClick: (event, rowData) => {
                handleCancel(rowData);
              },
              position: "row",
            },
            {
              icon: EditOutlinedIcon,
              onClick: (event, rowData) => {
                handleModify(rowData);
              },
              position: "row",
            },
          ]}
        />
      </ThemeProvider>
    );
  };

  const _renderListGender = () => {
    return (
      <>
        <option hidden>Gender</option>
        {GENDER.map((item: any) => (
          <option value={item.code} key={item.code}>
            {item.name}
          </option>
        ))}
      </>
    );
  };

  const _renderListDepartment = () => {
    return (
      <>
        <option hidden>Department</option>
        {departmentList.length > 0 ? (
          departmentList.map((item: any) => (
            <option value={item.code} key={item.code}>
              {item.name}
            </option>
          ))
        ) : (
          <option disabled>No option</option>
        )}
      </>
    );
  };

  const _renderSearch = () => {
    return (
      <div className="row">
        <div className="col-8 row">
          <div className="col-4">
            <input
              type="text"
              placeholder="Name"
              onChange={(e) => handleChangeName(e.target.value)}
              value={name}
              className="form-control"
            />
          </div>
          <div className="col-4">
            <select
              className="form-select"
              onChange={(e) => handleChangeGender(e.target.value)}
            >
              {_renderListGender()}
            </select>
          </div>
          <div className="col-4">
            <select
              className="form-select"
              onChange={(e) => handleChangeGender(e.target.value)}
            >
              {_renderListDepartment()}
            </select>
          </div>
        </div>
        <div className="col-4">
          <button className="button-apply">Apply</button>
        </div>
      </div>
    );
  };

  return (
    <Layout>
      {isOpenDetail ? (
        <Detail />
      ) : (
        <>
          <TotalView />
          <div className="d-flex justify-content-end me-4">
            <button
              className="button button--small button--primary"
              onClick={() => setIsOpenDetail(true)}
            >
              <i className="bi bi-plus"></i> Add
            </button>
          </div>
          <section className="table-container">
            <div className="table-container-contain">
              <div className="d-flex justify-content-center align-item-center">
                <h6 className="mb-0 text-center fw-bold p-3">
                  List of Patient
                </h6>
              </div>
              <div>
                <div className="container-search">{_renderSearch()}</div>
                <div>{_renderTableListPatient()}</div>
                <PaginationComponent
                  totalItem={totalItem}
                  itemPerPage={itemPerPage}
                  currentPage={currentPage}
                  getItemPerPage={getItemPerPage}
                  getCurrentPage={getCurrentPage}
                />
              </div>
            </div>
          </section>
          {showPopUpConfirm && (
            <PopUpConfirm handleCloseConfirmPopup={setShowPopUpConfirm} />
          )}
        </>
      )}
    </Layout>
  );
};

export default memo(Patient);
