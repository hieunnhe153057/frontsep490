import LOGO from "./images/logo.png";
import LOGO_HOSPITAL from "./images/logoHospital.png";

import {ReactComponent as ICON_DASHBOARD} from "./icons/dashboard.svg";
import {ReactComponent as ICON_DEPARTMENT} from "./icons/department.svg";
import {ReactComponent as ICON_DOCTOR} from "./icons/doctor.svg";
import {ReactComponent as ICON_PATIENT} from "./icons/patient.svg";
import {ReactComponent as ICON_STAFF} from "./icons/staff.svg";
import {ReactComponent as ICON_DIAGNOSTIC} from "./icons/diagnostic.svg";
import {ReactComponent as ICON_APPOINTMENT} from "./icons/appointment.svg";
import {ReactComponent as ICON_TRASH} from "./icons/trash.svg";
import {ReactComponent as ICON_PENCIL} from "./icons/pencil.svg";

export {
  LOGO,
  LOGO_HOSPITAL,
  ICON_DASHBOARD,
  ICON_DEPARTMENT,
  ICON_DOCTOR,
  ICON_PATIENT,
  ICON_STAFF,
  ICON_APPOINTMENT,
  ICON_DIAGNOSTIC,
  ICON_TRASH,
  ICON_PENCIL,
};
